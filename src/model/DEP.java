/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;
import edu.unb.fga.dadosabertos.Detalhes;
import edu.unb.fga.dadosabertos.Partido;
//import java.awt.List;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.xml.bind.JAXBException;
import java.util.List;
import java.util.*;
import view.IntDEPUI;
/**
 *
 * @author Pedro
 */
public class DEP  {

    private String[] nomes;
    private String[] partidos;
    private String[] emails;
    private String[] condicao;
    private String[] telefone;
    private String[] uf;
    private List tudo;
   
    public DEP() throws JAXBException, IOException{
             
    }
    public List getNomeDEP() throws JAXBException, IOException{
    
        
          Camara camara = new Camara();
          camara.obterDados(); 
          Deputado deputado = new Deputado();
          List<Deputado> deputados = camara.getDeputados();
          Detalhes detalhes = new Detalhes();
          Partido partido = new Partido();
          this.nomes = new String[deputados.size()];
          this.partidos = new String[deputados.size()];
          this.emails = new String[deputados.size()];
          this.condicao = new String[deputados.size()];
          this.telefone = new String[deputados.size()];
          this.uf = new String[deputados.size()];
         for(int i=0;i<deputados.size();i++){
            deputado = deputados.get(i); 
            deputado.obterDetalhes();
            detalhes = deputado.getDetalhes();
            partido = detalhes.getPartido();
            nomes[i] = deputado.getNome();
            partidos[i] = partido.getSigla();
            uf[i] = deputado.getUf();
            emails[i] = deputado.getEmail();
            telefone[i] = deputado.getFone();
            condicao[i] = deputado.getCondicao();
            
           
         }
         
         List<String> Name = new ArrayList<>(Arrays.asList(nomes));
         List<String> Part = new ArrayList<>(Arrays.asList(partidos));
         List<String> UF = new ArrayList<>(Arrays.asList(uf));
         List<String> Email = new ArrayList<>(Arrays.asList(emails));
         List<String> Fone = new ArrayList<>(Arrays.asList(telefone));
         List<String> Condi = new ArrayList<>(Arrays.asList(condicao));
         List<String> newList = new ArrayList<>(Name.size());
            int length = Name.size();
            for(int i=0;i<length;i++){
                newList.add(Name.get(i) + "\n");
                newList.add(Part.get(i));
                newList.add(UF.get(i));
                newList.add(Email.get(i));
                newList.add(Fone.get(i));
                newList.add(Condi.get(i));
            }
            this.tudo = newList;
            
         //System.out.println(newList);
         return tudo;
    }
         
        
     
    
    public static void main(String[] args) throws JAXBException, IOException {
        IntDEPUI dados = new IntDEPUI();
        dados.setVisible(true);
        
      
      
    }
    
}
